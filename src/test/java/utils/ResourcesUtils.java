package utils;

import model.data.RegionDomainsData;

import java.io.IOException;

import static utils.IOUtils.PROJECT_ABSOLUTE_PATH;

public class ResourcesUtils {

    private static final String RESOURCES_DIRECTORY = PROJECT_ABSOLUTE_PATH + "/src/test/resources";

    public static RegionDomainsData readRegionDomainsData() throws IOException {
        String absolutePath = RESOURCES_DIRECTORY + "/data/region_domains.json";
        return IOUtils.readJsonFileAsObject(absolutePath, RegionDomainsData.class);
    }
}
