package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class IOUtils {

    public static final String PROJECT_ABSOLUTE_PATH = new File(System.getProperty("user.dir")).getAbsolutePath();
    public static final String PROJECT_ROOT_PATH = new File(System.getProperty("user.dir")).getName();

    public static <T> T readJsonFileAsObject(String filePath, Class<T> objectClass) throws IOException {
        File file = new File(filePath);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(file, objectClass);
    }

    public static void writeFile(File file, String filepath) throws IOException {
        FileUtils.copyFile(file, new File(filepath));
    }
}
