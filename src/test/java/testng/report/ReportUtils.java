package testng.report;

import org.testng.Reporter;
import utils.IOUtils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static utils.IOUtils.PROJECT_ABSOLUTE_PATH;
import static utils.IOUtils.PROJECT_ROOT_PATH;

public class ReportUtils {

    public static void addScreenshotToReport(File screenshot, String testName) {
        try {
            String path = generateScreenshotFilePath(testName);
            IOUtils.writeFile(screenshot, PROJECT_ABSOLUTE_PATH + path);
            String message = String.format("<a href='/%1$s' target='_blank'><img src='/%1$s' width='50%%'/></a>",
                    PROJECT_ROOT_PATH + path);
            Reporter.log(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String generateScreenshotFilePath(String testName) {
        String reportDirectory = "/target/surefire-reports";
        return String.format("%s/failure_screenshots/%s.png", reportDirectory, generateScreenshotFileName(testName));
    }

    private static String generateScreenshotFileName(String testName) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_hhmm_");
        return currentDateTime.format(formatter) + testName.replaceAll("\\W", "");
    }
}
