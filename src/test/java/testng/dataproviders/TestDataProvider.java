package testng.dataproviders;

import model.data.RegionDomainsData;
import org.testng.annotations.DataProvider;

import java.util.Arrays;

public class TestDataProvider {

    @DataProvider(name = "allDomainData")
    public static Object[][] getAllDomain() {
        return RegionDomainsData.getInstance().getData();
    }

    @DataProvider(name = "onlyRegionDomainData")
    public static Object[][] getRegionDomain() {
        String[][] data = RegionDomainsData.getInstance().getData();
        return Arrays.copyOfRange(data, 1, data.length);
    }
}
