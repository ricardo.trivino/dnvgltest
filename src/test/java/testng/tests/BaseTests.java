package testng.tests;

import lombok.Getter;
import model.DriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import utils.ResourcesUtils;

import java.io.IOException;

public abstract class BaseTests {

    @Getter
    protected WebDriver driver;

    @BeforeSuite
    public void beforeSuite() throws IOException {
        ResourcesUtils.readRegionDomainsData();
    }

    @BeforeMethod
    public void createDriver() {
        driver = DriverManager.getDriver();
    }

    @AfterMethod
    public void tearDownDriver() {
        if (driver != null) {
            driver.quit();
        }
    }
}
