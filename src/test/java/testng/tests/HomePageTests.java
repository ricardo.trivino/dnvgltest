package testng.tests;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import testng.dataproviders.TestDataProvider;
import testng.listeners.ScreenshotListener;
import ui.navigation.Navigation;
import ui.pageobjects.header.RegionSelector;
import ui.pageobjects.pages.HomePage;

@Listeners(ScreenshotListener.class)
public class HomePageTests extends BaseTests{

    @Test(dataProvider = "allDomainData", dataProviderClass = TestDataProvider.class)
    public void homePageShouldDisplayAllElements(String region, String domain) {
        Navigation navigate = new Navigation(driver);
        HomePage homePage = navigate.goToHomePage(region);
        SoftAssert soft = new SoftAssert();
        soft.assertTrue(homePage.getHeader().areBasicElementsDisplayed(), "Header should be displayed");
        soft.assertTrue(homePage.isContentDisplayed(), "Page content should be displayed");
        soft.assertTrue(homePage.getFooter().areBasicElementsDisplayed(), "Social Media should be displayed");
        soft.assertAll();
    }

    @Test(dataProvider = "onlyRegionDomainData", dataProviderClass = TestDataProvider.class)
    public void allRegionalSitesShouldBeAccessibleFromGlobalHomePage(String region, String domain) {
        Navigation navigate = new Navigation(driver);
        HomePage homePage = navigate.goToHomePage("Global");
        if(!homePage.isDisplayed(RegionSelector.BUTTON)) {
            throw new NoSuchElementException("Region Selector is not displayed");
        }
        if (!homePage.isDisplayed(RegionSelector.DROPDOWN)) {
            homePage.getHeader().getRegionSelector().click();
        }
        homePage.getHeader().getRegionSelector().selectRegion(region);
        Assert.assertTrue(navigate.wait.untilUrlContains(domain));
    }
}
