package testng.listeners;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
import testng.report.ReportUtils;
import testng.tests.BaseTests;

import java.io.File;

public class ScreenshotListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        Object currentClass = result.getInstance();
        WebDriver driver = ((BaseTests) currentClass).getDriver();

        if (driver != null) {
            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String methodName = result.getMethod().getMethodName();
            methodName += "_" + result.getMethod().getParameterInvocationCount();
            ReportUtils.addScreenshotToReport(screenshot, methodName);
        }
    }

}
