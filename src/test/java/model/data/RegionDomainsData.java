package model.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RegionDomainsData {

    private static RegionDomainsData instance = null;
    private String[][] data;

    private RegionDomainsData() {
        super();
    }

    @JsonCreator
    public static RegionDomainsData getInstance() {
        if (instance == null) {
            synchronized(RegionDomainsData.class) {
                if (instance == null) {
                    instance = new RegionDomainsData();
                }
            }
        }
        return instance;
    }

    public String getDomain(String region) {
        for (String[] row : data) {
            if (row[0].equalsIgnoreCase(region)) {
                return row[1];
            }
        }
        throw new ArrayIndexOutOfBoundsException(region + " is not in the data set.");
    }
}
