package ui.pageobjects.pages;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ui.pageobjects.BasePageObject;
import ui.pageobjects.footer.Footer;
import ui.pageobjects.header.Header;

@Getter
public class HomePage extends BasePageObject {

    public static final By SLIDES = By.cssSelector("section.hero-carousel,div.flexslider-viewport");
    public static final By MAIN_TOPICS = By.cssSelector("section.main-topic,section.link-tiles");

    private final Header header;
    private final Footer footer;

    public HomePage(WebDriver driver) {
        super(driver);
        header = new Header(driver);
        footer = new Footer(driver);
    }


    //Actions

    public boolean isContentDisplayed() {
        return isDisplayed(SLIDES)
                && isDisplayed(MAIN_TOPICS);
    }
}
