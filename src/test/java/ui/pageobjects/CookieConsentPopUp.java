package ui.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CookieConsentPopUp extends BasePageObject {

    public static final By POP_UP = By.cssSelector("aside.cookie-consent");

    public CookieConsentPopUp(WebDriver driver) {
        super(driver);
    }
}
