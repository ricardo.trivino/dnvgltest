package ui.pageobjects.footer;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ui.pageobjects.BasePageObject;

public class Footer extends BasePageObject {

    public static final By SOCIAL_MEDIA = By.cssSelector("footer section.follow-links");
    public static final By ABOUT_LINKS = By.xpath("(//footer//section[contains(@class,'link-list')])[1]//li");
    public static final By CONTACT_LINKS = By.xpath("(//footer//section[contains(@class,'link-list')])[2]//li");
    public static final By LEGAL_LINKS = By.xpath("(//footer//section[contains(@class,'link-list')])[3]//li");

    public Footer(WebDriver driver) {
        super(driver);
    }

    //Actions

    public boolean areBasicElementsDisplayed() {
        return isDisplayed(SOCIAL_MEDIA)
                && isDisplayed(ABOUT_LINKS)
                && isDisplayed(CONTACT_LINKS)
                && isDisplayed(LEGAL_LINKS);
    }
}
