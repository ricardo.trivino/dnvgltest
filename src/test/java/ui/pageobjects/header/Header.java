package ui.pageobjects.header;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ui.pageobjects.BasePageObject;

public class Header extends BasePageObject {

    public static final By LOGO_LINK = By.xpath("//a[contains(@class, 'logo')]");
    public static final By NAVBAR_LINKS = By.xpath(
            "//a[@class='the-header__nav-item']" +
            "|//a[contains(@class, 'dnvgl-header-link')]");
    public static final By LOGIN_LINK = By.xpath("//a[contains(@class, 'login')]");
    public static final By SEARCH_ICON = By.xpath(
            "//button[contains(@class,'the-header__panel-toggle--search')]" +
            "|//div[contains(@class, 'dnvgl-branding-search')]");

    @Getter
    private final RegionSelector regionSelector;

    public Header(WebDriver driver) {
        super(driver);
        regionSelector = new RegionSelector(driver);
    }

    //Actions

    public boolean areBasicElementsDisplayed() {
        return isDisplayed(LOGO_LINK)
                && isDisplayed(NAVBAR_LINKS)
                && isDisplayed(LOGIN_LINK)
                && isDisplayed(SEARCH_ICON)
                && isDisplayed(RegionSelector.BUTTON);
    }
}
