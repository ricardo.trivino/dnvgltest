package ui.pageobjects.header;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ui.pageobjects.BasePageObject;

public class RegionSelector extends BasePageObject {

    public static final By BUTTON = By.xpath(
            "//a[contains(@class,'region-selection__btn')]" +
            "|//a[@href='/CountryOverview/']");
    public static final By DROPDOWN = By.xpath(
            "//section[@class='region-selection__nav-scroll']" +
            "|//div[contains(@class, 'panel')]");
    public static final By OPTIONS = By.xpath("" +
            "//nav[@role='region']//li/a" +
            "|//div[contains(@class, 'localwebsites')]//li/a");


    public RegionSelector(WebDriver driver) {
        super(driver);
    }


    //Actions

    public void click() {
        driver.findElement(BUTTON).click();
    }

    public void selectRegion(String region) {
        for (WebElement regionLink : driver.findElements(OPTIONS)) {
            if (regionLink.getText().equalsIgnoreCase(region)) {
                scrollToVisible(regionLink).click();
                return;
            }
        }
        throw new ArrayIndexOutOfBoundsException(region + " is not on the list");
    }
}
