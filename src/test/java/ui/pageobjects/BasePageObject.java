package ui.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import ui.wait.WaitManager;

import java.util.List;

public abstract class BasePageObject {

    protected WebDriver driver;
    protected WaitManager wait;

    protected BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.wait = new WaitManager(driver, 5);
        wait.untilPageLoad();
    }

    protected WebElement scrollToVisible(WebElement element) {
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);", element);
        return element;
    }

    public boolean isDisplayed(By locator) {
        List<WebElement> elements = driver.findElements(locator);
        return elements.size() > 0
                && elements.stream().allMatch(WebElement::isDisplayed);
    }
}
