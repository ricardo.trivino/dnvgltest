package ui.navigation;

import model.data.RegionDomainsData;
import org.openqa.selenium.WebDriver;
import ui.pageobjects.pages.HomePage;
import ui.wait.WaitManager;

public class Navigation {

    private final WebDriver driver;
    private String domain;

    public WaitManager wait;

    public Navigation(WebDriver driver) {
        this.driver = driver;
        this.wait = new WaitManager(driver, 5);
    }

    //GoTo

    private void goTo(String path) {
        driver.get(domain + path);
    }

    public HomePage goToHomePage(String region) {
        domain = RegionDomainsData.getInstance().getDomain(region);
        goTo("/");
        return onHomePage();
    }


    //On

    public HomePage onHomePage() {
        return new HomePage(driver);
    }
}
