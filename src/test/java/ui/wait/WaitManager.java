package ui.wait;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitManager extends WebDriverWait {

    public WaitManager(WebDriver driver, long timeOut) {
        super(driver, timeOut);
    }

    public void untilPageLoad() {
        until(new ExpectedConditionJavaScriptLoad());
    }

    public Boolean untilUrlContains(String url) {
        return until(ExpectedConditions.urlContains(url));
    }
}
