
# DNV-GL

This is a test project in Java with Maven, using TestNG with Selenium to do some UI test on web https://www.dnvgl.com/


### Prerequisites

You need Java 8 or higher to run these tests. To launch test locally, you also need to have installed Maven and Docker Compose.


### Running the tests

First, from the project's directory, launch the dockerized Selenium Hub with the following command
```
docker-compose -f ./docker-compose.yml up -d
```
This will start a hub with one node containing 9 instances of Chrome browser.
To stop it after the tests, you just need to run
```
docker-compose stop
```
To run the tests, you just need to run Maven command
```
mvn clean test
```
This will run the test listed in the file `src/test/resources/testng/sanity_suite.xml`.


### Two different branches

The project has a `master` branch, with the tests in sequential execution, and a `Parallel` branch, with the tests run in parallel.

There are two simple parameterized tests, with 38 iterations, one for each country of the site. One of the test check that the links in the Global page redirect correctly to the regional websites. The other one checks that all the sites, included the Global homepage, display all the main elements on the header, the body and the footer.

Run in sequence they take some time... The parallel approach looks like the perfect one, but it has a bug with the take of the screenshot, as it doesn't take the correct one.


### Release History

* 0.0.3
    * Work in progress
